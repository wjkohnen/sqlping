# Sqlping

Sqlping is a _very_ simple command that checks if a TCP connection to either a
Postres or a MySQL database instance is possible to be established. Only
prerequisite is a valid database user / password that is allowed to connect to
the instance. Other permissions are not necessary. 

On success there is no output and the exit code is zero, otherwise there is
some error message output and a non-zero exit code. 

## Usage

```
$ sqlping --help
Usage of sqlping:
  -driver string
    	driver {mysql,postgres} (default "mysql")
  -host string
    	host (default "localhost")
  -password string
    	password
  -port int
    	port; 0 means 3306 on mysql, 5432 on postgres
  -timeout duration
    	timeout (default 3s)
  -user string
    	user (default "ping")
```

## Build from source

```bash
$ go get github.com/golang/dep/cmd/dep
$ go get -d gitlab.com/wjkohnen/sqlping
$ cd ~/go/src/gitlab.com/wjkohnen/sqlping
$ dep ensure --vendor-only
$ go install
```

## License

Copyright (c) 2018 Johannes Kohnen <gpl3-2017@ko-sys.com>

This file is part of sqlping.

sqlping is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

sqlping is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
sqlping.  If not, see <http://www.gnu.org/licenses/>.
